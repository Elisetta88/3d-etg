## Script to validate other tools predictions (GTEx,PanCan) ##
library(data.table)
library(GenomicRanges)
library(biomaRt)

### ------------ load utility functions ------------ ###
source("/storage/home/esalviat/MatrixETG/CanonicalCorrelation_Version_20190627/UtilityFunction_CanonicalCorrelation.R")


###### ---- 17 Dec 2020 ---- #######
# There are 7 methods for which I have the predictions and I can evaluate them ###
# Review: https://www.sciencedirect.com/science/article/pii/S2001037019303630#b0580

# dir("/storage/home/esalviat/MatrixETG/OtherToolPredictions/")
method<-c("JEME","TargetFinder","PETmodule","RIPPLE","PRESTIGE","DeepTACT")
dir.result<-"/storage/home/esalviat/MatrixETG/OtherToolPredictions/ToolPrediction_refined/" 

print(load("/storage/home/esalviat/MatrixETG/OtherToolPredictions/ToolPrediction_refined/UCSC_exons.RData"))

## load each list and format it in the same way
# E.info (chr:start-end)
# P.info or TSS
# G.symbol
# Predicted: 0-No; 1:Yes
# Distance (between Enhancer-TSS or Enhancer-mid.promoter)
# Other (ie, G.symbol, score, G.strand ..)


### ----- FOCS ----- ###
dir.pred<-"/storage/home/esalviat/MatrixETG/OtherToolPredictions/FOCS/"
pred.i<- get(load(paste0(dir.pred,"FOCS_20200503.RData")))

pred.i$Distance<-( pred.i$P.start + (pred.i$P.end-pred.i$P.start)/2  )-
		   ( pred.i$E.start + (pred.i$E.end-pred.i$E.start)/2)

U.gene<-strsplit(pred.i$P.symbol,split=";")
n<-sapply(U.gene,length)
IND<-rep(1:length(U.gene),times=n)

pred.unique.gene<-pred.i[IND,]
pred.unique.gene$G.symbol<-unlist(U.gene)
pred.unique.gene$Index<-IND

# Validation
E.tab<-pred.unique.gene[,c("E.chr","E.start","E.end")]	
E.info<-paste0(E.tab$E.chr,":",E.tab$E.start,"-",E.tab$E.end)	
Pred.EG.code<-paste(E.info,pred.unique.gene$G.symbol,sep=";")
P.tab<-pred.unique.gene[,c("P.chr","P.start","P.end")]	

TSS<-data.table(
	chr=P.tab$P.chr,
	TSS=round(as.integer(P.tab$P.start)+( (as.integer(P.tab$P.end)-as.integer(P.tab$P.start)) /2)),
	G.symbol=pred.unique.gene$G.symbol
)

data.i<-cbind(E.tab,P.tab,Pred.EG.code,TSS,Predicted=pred.unique.gene$Predicted)
data.i$E.info<-paste0(E.tab$E.chr,":",E.tab$E.start,"-",E.tab$E.end)

	## --- Refine enhancer list --- ##
	E.GR<-reduce(unique(GRanges(
		seqnames=Rle(E.tab$E.chr),
		ranges=IRanges(start=as.integer(paste0(E.tab$E.start)),end=as.integer(paste0(E.tab$E.end)) ),
		strand=Rle(rep("*",nrow(E.tab)) )
	)))
	E.GR$code<-paste0(E.GR)

	x.ref<-setdiff(E.GR,GR.exon)
	x.ref$code.new<-paste0(x.ref)
	
	ind.ref<-findOverlaps(x.ref,E.GR,type="within")
	x.ref$code.old<-E.GR$code[ind.ref@to]
	x.ref<-x.ref[which(x.ref@ranges@width>=10)]

	pairs.i.refined<-data.table::data.table(E.info.new=x.ref$code.new,E.info=x.ref$code.old)
	pairs.i.refined<-merge(pairs.i.refined,data.i,by="E.info",allow.cartesian=TRUE)

	## recompute distance; change name
	colnames(pairs.i.refined)[which(colnames(pairs.i.refined)=="E.info")]<-"E.info.old"
	#colnames(pairs.i.refined)[which(colnames(pairs.i.refined)=="distance")]<-"Distance.old"
	colnames(pairs.i.refined)[which(colnames(pairs.i.refined)=="E.info.new")]<-"E.info"

	E.tab<-data.table(do.call("rbind",strsplit(pairs.i.refined$E.info,split="[:-]")))
	colnames(E.tab)<-c("E.chr","E.start","E.end")
	pairs.i.refined$distance<-as.integer(pairs.i.refined$TSS)-round(as.integer(E.tab$E.start)+(as.integer(E.tab$E.end) -as.integer(E.tab$E.start))/2)


save(pairs.i.refined,file=paste0(dir.result,"Prediction_FOCS_refined.RData"))



### ----- JEME ----- ###
## http://yiplab.cse.cuhk.edu.hk/jeme/
# All coordinates are based on the hg19 human reference genome. 
#In each file, each line corresponds to an enhancer-target pair, where:
# 1) enhancer (0-based, left-close, right-open)
# 2) transcripts sharing the TSS separated by %
# 3) The third field is the confidence score of the enhancer-target connection, with a higher score indicating a more confident prediction 

dir.pred<-"/storage/home/esalviat/MatrixETG/OtherToolPredictions/JEME/encoderoadmap_lasso/"
dir.active<-"/storage/home/esalviat/MatrixETG/OtherToolPredictions/JEME/active/"
name.files<-dir(dir.pred)
# cut-off 0.35: all the reported interactions are teh predicted ones

Result.JEME<-vector("list",length(name.files))
names(Result.JEME)<-name.files

for(i in 1:length(name.files)){
	
	cat(i,".. ")
	data.i<-data.table::fread(paste0(dir.pred,name.files[i]))
	colnames(data.i)<-c("E.info","P.info","score")
	# 1. check multiple trasnscripts
	ind.mult<-grep("%",data.i$P.info)
	if(length(ind.mult)>0 ) warning("Dividi le righe")
	# 2. organize information
	P.tab<-data.table(do.call("rbind",strsplit(data.i$P.info,split="\\$")))
	colnames(P.tab)<-c("G.code","G.symbol","chr","TSS","strand")

	data.i<-cbind(data.i,P.tab)
	
	## check candidate ##
	## -- JEME candidate -- ##
	#2) Roadmap/2_second_step_modeling/active
	#In this directory, each comma-separated file corresponds to one sample and has the following format:
	#. Each row corresponds to one enhancer-TSS pair, and the rows cover all active enhancers in this sample and all TSSs within 1Mbp from each of them.
	#. Each row contains five columns, namely i) pair ID, ii) enhancer ID, iii) TSS ID, iv) genomic distance between the enhancer and the TSS, and v) a dummy random class label (for defining the classes, the actual values not really used in training or testing)
	#For example, for sample 1, the comma-separated file is named "1.active", and the first line is for one particular enhancer-TSS pair:
	#chr10:100049400-100050000_ENSG00000052749.9$RRP12$chr10$99161127$-,chr10:100049400-100050000,ENSG00000052749.9$RRP12$chr10$99161127$-,888573,0

	code.i<-strsplit(name.files[i],split="\\.")[[1]][2]
	name.pairs<-paste0(code.i,".active")#dir(dir.active)[grep(paste0(code.i,'\\.'),dir(dir.active))]
	pairs.i<-data.table::fread(file=paste0(dir.active,name.pairs))[,-1]
	colnames(pairs.i)<-c("E.info","P.info","distance","label")

	P.pairs<-data.table(do.call("rbind",strsplit(pairs.i$P.info,split="\\$")))
	colnames(P.pairs)<-c("G.code","G.symbol","chr","TSS","strand")
	pairs.i<-cbind(pairs.i,P.pairs)
	
	c1<-paste(pairs.i$P.info,pairs.i$E.info,sep=";")
	c.sub<-paste(data.i$P.info,data.i$E.info,sep=";")
	if( sum(c.sub %in% c1)!=length(c.sub) ) {
		warning("Not all predcited pairs are in candidate pairs")
		next
	}
	ind.is.pred<-match(c.sub,c1)#pbapply::pblapply(c.sub,function(x) which(x==c1))
	pairs.i$Predicted<- rep(0,length(c1))#as.numeric(c1 %in% c.sub)
	pairs.i$Predicted[ind.is.pred]<-1

	pairs.i$score<- rep(0,length(c1))#
	pairs.i$score[ind.is.pred]<-data.i$score

	## --- Refine enhancer list --- ##
	E.tab<-data.table(do.call("rbind",strsplit(pairs.i$E.info,split="[:-]")))
	colnames(E.tab)<-c("E.chr","E.start","E.end")

	E.GR<-reduce(unique(GRanges(
		seqnames=Rle(E.tab$E.chr),
		ranges=IRanges(start=as.integer(paste0(E.tab$E.start)),end=as.integer(paste0(E.tab$E.end)) ),
		strand=Rle(rep("*",nrow(E.tab)) )
	)))
	E.GR$code<-paste0(E.GR)

	x.ref<-setdiff(E.GR,GR.exon)
	x.ref$code.new<-paste0(x.ref)
	
	ind.ref<-findOverlaps(x.ref,E.GR,type="within")
	x.ref$code.old<-E.GR$code[ind.ref@to]
	x.ref<-x.ref[which(x.ref@ranges@width>=10)]

	pairs.i.refined<-data.table::data.table(E.info.new=x.ref$code.new,E.info=x.ref$code.old)
	pairs.i.refined<-merge(pairs.i.refined,pairs.i,by="E.info",allow.cartesian=TRUE)

	## recompute distance; change name
	colnames(pairs.i.refined)[which(colnames(pairs.i.refined)=="E.info")]<-"E.info.old"
	colnames(pairs.i.refined)[which(colnames(pairs.i.refined)=="distance")]<-"Distance.old"
	colnames(pairs.i.refined)[which(colnames(pairs.i.refined)=="E.info.new")]<-"E.info"

	E.tab<-data.table(do.call("rbind",strsplit(pairs.i.refined$E.info,split="[:-]")))
	colnames(E.tab)<-c("E.chr","E.start","E.end")
	pairs.i.refined$distance<-as.integer(pairs.i.refined$TSS)-round(as.integer(E.tab$E.start)+(as.integer(E.tab$E.end) -as.integer(E.tab$E.start))/2)



	#Val.EG.code<-paste(paste0(E.GR[E.ind@to]),Validation$symbol[E.ind@from],sep=";")

	## convert QTl in ETG, mapping SNP within defned enhancers
	#E.tab<-data.table(do.call("rbind",strsplit(pairs.i$E.info,split="[:-]")))
	#colnames(E.tab)<-c("E.chr","E.start","E.end")
	#Pred.EG.code<-paste(pairs.i$E.info,pairs.i$G.symbol,sep=";")
	
	#TT<-data.table(sapply(Validated,get.ETG,Enhancer=E.tab,Predicted=Pred.EG.code))
	#pairs.i<-cbind(pairs.i,TT)

	#pairs.i$distance<-as.integer(pairs.i$TSS)-
	#	round(as.integer(E.tab$E.start)+(as.integer(E.tab$E.end) -as.integer(E.tab$E.start))/2)
	colnames(pairs.i.refined)[3]<-"G.info"

	Result.JEME[[i]]<-pairs.i.refined
}

save(Result.JEME,file=paste0(dir.result,"Prediction_JEME_refined.RData"))





### --- TargetFinder --- ###
# original(label): 1: predicted; 0; not-predicted
# original(prediction): 1:validated; 0:not validated

dir.pred<-"/storage/home/esalviat/MatrixETG/OtherToolPredictions/TargetFinder/Prediction/"
cell.lines<-unique(sapply(strsplit(dir(dir.pred),split="_"),function(x) x[1]))

# Convert ENSG ID to Symbol (required for eQTL validtion) 
ensembl_hg19 = useMart(biomart="ENSEMBL_MART_ENSEMBL", 
	host="grch37.ensembl.org", 
	path="/biomart/martservice", 
	dataset="hsapiens_gene_ensembl")
#listAttributes(ensembl_hg19)[1:10,]
#searchFilters(mart = ensembl_hg19, pattern = "id")

Result.TargetFinder<-vector("list",length(cell.lines))
names(Result.TargetFinder)<-cell.lines

for(i in 1:length(cell.lines)){
	
	name.file.tss<-find.Files(dir.pred,key=c(cell.lines[i],"tss"))
	name.file.pred<-find.Files(dir.pred,key=c(cell.lines[i],"predictions"))

	tss.i<-data.table::fread(paste0(dir.pred,name.file.tss))
	colnames(tss.i)<-c("chr","start","end","G.ensembl")
	
	pre.i<-data.table::fread(paste0(dir.pred,name.file.pred))
	pre.i$enhancer_name<-sapply(strsplit(pre.i$enhancer_name,split="\\|"),function(x) x[2])
	pre.i$promoter_name<-sapply(strsplit(pre.i$promoter_name,split="\\|"),function(x) x[2])
	
	## note: some genes can have multiple link (not accounted)
	## --- get symbol using ensembl with version  --- ##
	G.info<-data.table::data.table(
		getBM(filters= "ensembl_gene_id_version", 
		attributes= c("ensembl_gene_id","hgnc_symbol","ensembl_gene_id_version","strand"),
		values=tss.i$G.ensembl,mart= ensembl_hg19))
	# remove genes with no symbol
	G.info<-G.info[G.info$hgnc_symbol!="",]
	
	ind<-match(tss.i$G.ensembl,G.info$ensembl_gene_id_version)
	tss.i$G.symbol<-G.info$hgnc_symbol[ind]
	GR.tss<-GRanges(
		seqnames=Rle(tss.i$chr),
		ranges=IRanges(start=tss.i$start,end=tss.i$end),
		strand=Rle(rep("*",nrow(tss.i))),
		G.symbol=G.info$hgnc_symbol[ind]
	)

	## --- link tss with its promoter --- ##
	## for each TSS there is only one promoter;
	## for each promoter more than one TSS
	# (with unique P.GR) table(table(ind@to)); sum(table(table(ind@from)))

	P.tab<-data.table(do.call("rbind",strsplit(pre.i$promoter_name,split="[:-]")))
	colnames(P.tab)<-c("P.chr","P.start","P.end")
	P.GR<-GRanges(
		seqnames=Rle(P.tab$P.chr),
		ranges=IRanges(start=as.integer(paste0(P.tab$P.start)),end=as.integer(paste0(P.tab$P.end)) ),
		strand=Rle(rep("*",nrow(P.tab)) )
	)

	ind<-findOverlaps(P.GR,GR.tss)	
	pre.i.single.gene<-cbind(pre.i[ind@from,],tss.i[ind@to,])
	pre.i.single.gene<-pre.i.single.gene[!is.na(pre.i.single.gene$G.symbol),]

	E.tab<-data.table(do.call("rbind",strsplit(pre.i.single.gene$enhancer_name,split="[:-]")))
	colnames(E.tab)<-c("E.chr","E.start","E.end")
	E.tab$E.mid<-round(as.integer(paste0(E.tab$E.end))+(as.integer(paste0(E.tab$E.end))-as.integer(paste0(E.tab$E.start))))

	pre.i.single.gene$Distance<-as.integer(round(pre.i.single.gene$start-E.tab$E.mid))

	ind.take<-which(!(colnames(pre.i.single.gene) %in% c("prediction","end")))
	pre.i.single.gene<-pre.i.single.gene[,..ind.take]
	colnames(pre.i.single.gene)<-c("E.info","P.info","Predicted","chr","TSS","G.code","G.symbol","Distance")

	#Result.TargetFinder[[i]]<-pre.i.single.gene


	## --- Refine enhancer list --- ##
	E.tab<-data.table(do.call("rbind",strsplit(pre.i.single.gene$E.info,split="[:-]")))
	colnames(E.tab)<-c("E.chr","E.start","E.end")

	E.GR<-reduce(unique(GRanges(
		seqnames=Rle(E.tab$E.chr),
		ranges=IRanges(start=as.integer(paste0(E.tab$E.start)),end=as.integer(paste0(E.tab$E.end)) ),
		strand=Rle(rep("*",nrow(E.tab)) )
	)))
	E.GR$code<-paste0(E.GR)

	x.ref<-setdiff(E.GR,GR.exon)
	x.ref$code.new<-paste0(x.ref)

	ind.ref<-findOverlaps(x.ref,E.GR,type="within")
	x.ref$code.old<-E.GR$code[ind.ref@to]
	x.ref<-x.ref[which(x.ref@ranges@width>=10)]
	pairs.i.refined<-data.table::data.table(E.info.new=x.ref$code.new,E.info=x.ref$code.old)
	
	pairs.i.refined<-merge(pairs.i.refined,pre.i.single.gene,by="E.info",allow.cartesian=TRUE)

	## recompute distance; change name
	colnames(pairs.i.refined)[which(colnames(pairs.i.refined)=="E.info")]<-"E.info.old"
	colnames(pairs.i.refined)[which(colnames(pairs.i.refined)=="Distance")]<-"Distance.old"
	colnames(pairs.i.refined)[which(colnames(pairs.i.refined)=="E.info.new")]<-"E.info"

	E.tab<-data.table(do.call("rbind",strsplit(pairs.i.refined$E.info,split="[:-]")))
	colnames(E.tab)<-c("E.chr","E.start","E.end")
	pairs.i.refined$Distance<-as.integer(pairs.i.refined$TSS)-round(as.integer(E.tab$E.start)+(as.integer(E.tab$E.end) -as.integer(E.tab$E.start))/2)


	Result.TargetFinder[[i]]<-pairs.i.refined

}
save(Result.TargetFinder,file=paste0(dir.result,"Prediction_TargetFinder_refined.RData"))



### --- PETmodule --- ###
dir.pred<-"/storage/home/esalviat/MatrixETG/OtherToolPredictions/PETmodule/Prediction/"
name.files<-dir(dir.pred)


Result.PETmodule<-vector("list",length(name.files))
names(Result.PETmodule)<-name.files

for(i in 1:length(name.files)){
	cat(i,".. ")
	data.i<-data.table::fread(paste0(dir.pred,name.files[i]))
	colnames(data.i)<-c("E.info","G.symbol","score")
	
	# mantain only genes with at least one prediction
	#ind.take<-which(data.i$G.symbol %in% unique(c(Validated$GTEX$symbol,Validated$PancanQTL$symbol)))
	#data.i<-data.i[ind.take,]

	## TSS and distance from enhancer
	G.info<-data.table::data.table(
		getBM(filters= "hgnc_symbol", 
		attributes= c( "hgnc_symbol","start_position","end_position","chromosome_name","transcription_start_site","strand"),
		values=unique(data.i$G.symbol),mart= ensembl_hg19))
	G.info<-G.info[G.info$chromosome_name %in% c(1:22,"X"), ]
	
	G.info$TSS<-rep(NA,nrow(G.info))
	G.info$TSS[G.info$strand==1]<-G.info$start_position[G.info$strand==1]
	G.info$TSS[G.info$strand==-1]<-G.info$end_position[G.info$strand==-1]
	G.info<-unique(G.info[,c(1,4,6,7)])

	ind.gene<-match(data.i$G.symbol,G.info$hgnc_symbol)
	data.i$TSS<-G.info$TSS[ind.gene]
	data.i$TSS.chr<-G.info$chromosome_name[ind.gene]

	data.i<-data.i[!is.na(data.i$TSS),]
	data.i$Predicted<-1
	data.i$TSS.chr<-paste0("chr",data.i$TSS.chr)
	
	## convert QTl in ETG, mapping SNP within defned enhancers
	E.tab<-data.table(do.call("rbind",strsplit(data.i$E.info,split="[:-]")))
	colnames(E.tab)<-c("E.chr","E.start","E.end")

	
	data.i$distance<-data.i$TSS-
		round(as.integer(paste0(E.tab$E.start))+(as.integer(paste0(E.tab$E.end)) -as.integer(paste0(E.tab$E.start)))/2)

	## some mostakes with biomart (wrong chromosome anotations)
	ind.correct<-which(E.tab$E.chr==data.i$TSS.chr)
	data.i<-data.i[ind.correct,]

	colnames(data.i)<-c("E.info","G.symbol","score","TSS","chr","Predicted","Distance")



	### --- Refine enhancer list --- ##
	E.tab<-data.table(do.call("rbind",strsplit(data.i$E.info,split="[:-]")))
	colnames(E.tab)<-c("E.chr","E.start","E.end")

	E.GR<-reduce(unique(GRanges(
		seqnames=Rle(E.tab$E.chr),
		ranges=IRanges(start=as.integer(paste0(E.tab$E.start)),end=as.integer(paste0(E.tab$E.end)) ),
		strand=Rle(rep("*",nrow(E.tab)) )
	)))
	E.GR$code<-paste0(E.GR)

	x.ref<-setdiff(E.GR,GR.exon)
	x.ref$code.new<-paste0(x.ref)
	
	ind.ref<-findOverlaps(x.ref,E.GR,type="within")
	x.ref$code.old<-E.GR$code[ind.ref@to]
	x.ref<-x.ref[which(x.ref@ranges@width>=10)]

	pairs.i.refined<-data.table::data.table(E.info.new=x.ref$code.new,E.info=x.ref$code.old)
	
	pairs.i.refined<-merge(pairs.i.refined,data.i,by="E.info",allow.cartesian=TRUE)

	## recompute distance; change name
	colnames(pairs.i.refined)[which(colnames(pairs.i.refined)=="E.info")]<-"E.info.old"
	colnames(pairs.i.refined)[which(colnames(pairs.i.refined)=="Distance")]<-"Distance.old"
	colnames(pairs.i.refined)[which(colnames(pairs.i.refined)=="E.info.new")]<-"E.info"

	E.tab<-data.table(do.call("rbind",strsplit(pairs.i.refined$E.info,split="[:-]")))
	colnames(E.tab)<-c("E.chr","E.start","E.end")
	pairs.i.refined$Distance<-as.integer(pairs.i.refined$TSS)-round(as.integer(E.tab$E.start)+(as.integer(E.tab$E.end) -as.integer(E.tab$E.start))/2)


	Result.PETmodule[[i]]<-pairs.i.refined
}

save(Result.PETmodule,file=paste0(dir.result,"Prediction_PETmodule_refined.RData"))



### --- RIPPLE --- ###
dir.pred<-"/storage/home/esalviat/MatrixETG/OtherToolPredictions/RIPPLE/Prediction/"
name.files<-dir(dir.pred)


Result.RIPPLE<-vector("list",length(name.files))
names(Result.RIPPLE)<-name.files

for(i in 1:length(name.files)){
	cat(i,".. ")
	data.i<-data.table::fread(paste0(dir.pred,name.files[i]))
	colnames(data.i)<-c("E.info","score","G.symbol")
	

	## TSS and distance from enhancer
	G.info<-data.table::data.table(
		getBM(filters= "hgnc_symbol", 
		attributes= c( "hgnc_symbol","start_position","end_position","chromosome_name","transcription_start_site","strand"),
		values=unique(data.i$G.symbol),mart= ensembl_hg19))
	G.info<-G.info[G.info$chromosome_name %in% c(1:22,"X"), ]

	
	G.info$TSS<-rep(NA,nrow(G.info))
	G.info$TSS[G.info$strand==1]<-G.info$start_position[G.info$strand==1]
	G.info$TSS[G.info$strand==-1]<-G.info$end_position[G.info$strand==-1]
	G.info<-unique(G.info[,c(1,4,6,7)])


	ind.gene<-match(data.i$G.symbol,G.info$hgnc_symbol)
	data.i$TSS<-G.info$TSS[ind.gene]
	data.i$TSS.chr<-G.info$chromosome_name[ind.gene]

	data.i<-data.i[!is.na(data.i$TSS),]
	data.i$Predicted<-1
	data.i$TSS.chr<-paste0("chr",data.i$TSS.chr)

	## convert QTl in ETG, mapping SNP within defned enhancers
	E.tab<-data.table(do.call("rbind",strsplit(data.i$E.info,split="[_]")))
	colnames(E.tab)<-c("E.chr","E.start","E.end")

	
	data.i$distance<-data.i$TSS-
		round(as.integer(paste0(E.tab$E.start))+(as.integer(paste0(E.tab$E.end)) -as.integer(paste0(E.tab$E.start)))/2)

	data.i$E.info<-paste0(E.tab$E.chr,":",E.tab$E.start,"-",E.tab$E.end)

	## some mostakes with biomart (wrong chromosome anotations)
	ind.correct<-which(E.tab$E.chr==data.i$TSS.chr)
	data.i<-data.i[ind.correct,]

	colnames(data.i)<-c("E.info","score","G.symbol","TSS","chr","Predicted","Distance")



	### --- Refine enhancer list --- ##
	E.tab<-data.table(do.call("rbind",strsplit(data.i$E.info,split="[:-]")))
	colnames(E.tab)<-c("E.chr","E.start","E.end")

	E.GR<-reduce(unique(GRanges(
		seqnames=Rle(E.tab$E.chr),
		ranges=IRanges(start=as.integer(paste0(E.tab$E.start)),end=as.integer(paste0(E.tab$E.end)) ),
		strand=Rle(rep("*",nrow(E.tab)) )
	)))
	E.GR$code<-paste0(E.GR)

	x.ref<-setdiff(E.GR,GR.exon)
	x.ref$code.new<-paste0(x.ref)
	
	ind.ref<-findOverlaps(x.ref,E.GR,type="within")
	x.ref$code.old<-E.GR$code[ind.ref@to]
	x.ref<-x.ref[which(x.ref@ranges@width>=10)]

	pairs.i.refined<-data.table::data.table(E.info.new=x.ref$code.new,E.info=x.ref$code.old)
	
	pairs.i.refined<-merge(pairs.i.refined,data.i,by="E.info",allow.cartesian=TRUE)

	## recompute distance; change name
	colnames(pairs.i.refined)[which(colnames(pairs.i.refined)=="E.info")]<-"E.info.old"
	colnames(pairs.i.refined)[which(colnames(pairs.i.refined)=="Distance")]<-"Distance.old"
	colnames(pairs.i.refined)[which(colnames(pairs.i.refined)=="E.info.new")]<-"E.info"

	E.tab<-data.table(do.call("rbind",strsplit(pairs.i.refined$E.info,split="[:-]")))
	colnames(E.tab)<-c("E.chr","E.start","E.end")
	pairs.i.refined$Distance<-as.integer(pairs.i.refined$TSS)-round(as.integer(E.tab$E.start)+(as.integer(E.tab$E.end) -as.integer(E.tab$E.start))/2)





	Result.RIPPLE[[i]]<-pairs.i.refined
}


save(Result.RIPPLE,file=paste0(dir.result,"Prediction_RIPPLE_refined.RData"))



### --- PreTSIGE --- ###
dir.pred<-"/storage/home/esalviat/MatrixETG/OtherToolPredictions/PreSTIGE/Prediction/"
name.files<-dir(dir.pred)

Result.PreSTIGE<-vector("list",length(name.files))
names(Result.PreSTIGE)<-name.files

for(i in 1:length(name.files)){
	cat(i,".. ")
	data.i<-data.table::fread(paste0(dir.pred,name.files[i]))
	data.i<-data.i[,c(2,1,3,4,5,6)]
	colnames(data.i)[1:2]<-c("E.info","G.symbol")
	

	## TSS and distance from enhancer
	G.info<-data.table::data.table(
		getBM(filters= "hgnc_symbol", 
		attributes= c( "hgnc_symbol","start_position","end_position","chromosome_name","transcription_start_site","strand","ensembl_transcript_id"),
		values=unique(data.i$G.symbol),mart= ensembl_hg19))
	G.info<-G.info[G.info$chromosome_name %in% c(1:22,"X","Y"), ]
	
	G.info$TSS<-rep(NA,nrow(G.info))
	G.info$TSS[G.info$strand==1]<-G.info$start_position[G.info$strand==1]
	G.info$TSS[G.info$strand==-1]<-G.info$end_position[G.info$strand==-1]
	G.info<-unique(G.info[,c(1,4,8)])

	ind.gene<-match(data.i$G.symbol,G.info$hgnc_symbol)
	data.i$TSS<-G.info$TSS[ind.gene]
	data.i$TSS.chr<-G.info$chromosome_name[ind.gene]

	data.i<-data.i[!is.na(data.i$TSS),]
	data.i$Predicted<-1
	data.i$TSS.chr<-paste0("chr",data.i$TSS.chr)


	## convert QTl in ETG, mapping SNP within defned enhancers
	E.tab<-data.table(do.call("rbind",strsplit(data.i$E.info,split="[:-]")))
	colnames(E.tab)<-c("E.chr","E.start","E.end")	
	
	data.i$distance<-data.i$TSS - round(as.integer(E.tab$E.start)+(as.integer(E.tab$E.end) -as.integer(E.tab$E.start))/2)

	## some mostakes with biomart (wrong chromosome anotations)
	ind.correct<-which(E.tab$E.chr==data.i$TSS.chr)
	data.i<-data.i[ind.correct,]

	colnames(data.i)<-c("E.info","G.symbol","gene_expr","me1_max","spec_score_expr","spec_score_me1","TSS","chr","Predicted","Distance")



	### --- Refine enhancer list --- ##
	E.tab<-data.table(do.call("rbind",strsplit(data.i$E.info,split="[:-]")))
	colnames(E.tab)<-c("E.chr","E.start","E.end")

	E.GR<-reduce(unique(GRanges(
		seqnames=Rle(E.tab$E.chr),
		ranges=IRanges(start=as.integer(paste0(E.tab$E.start)),end=as.integer(paste0(E.tab$E.end)) ),
		strand=Rle(rep("*",nrow(E.tab)) )
	)))
	E.GR$code<-paste0(E.GR)

	x.ref<-setdiff(E.GR,GR.exon)
	x.ref$code.new<-paste0(x.ref)
	
	ind.ref<-findOverlaps(x.ref,E.GR,type="within")
	x.ref$code.old<-E.GR$code[ind.ref@to]
	x.ref<-x.ref[which(x.ref@ranges@width>=10)]

	pairs.i.refined<-data.table::data.table(E.info.new=x.ref$code.new,E.info=x.ref$code.old)
	
	pairs.i.refined<-merge(pairs.i.refined,data.i,by="E.info",allow.cartesian=TRUE)

	## recompute distance; change name
	colnames(pairs.i.refined)[which(colnames(pairs.i.refined)=="E.info")]<-"E.info.old"
	colnames(pairs.i.refined)[which(colnames(pairs.i.refined)=="Distance")]<-"Distance.old"
	colnames(pairs.i.refined)[which(colnames(pairs.i.refined)=="E.info.new")]<-"E.info"

	E.tab<-data.table(do.call("rbind",strsplit(pairs.i.refined$E.info,split="[:-]")))
	colnames(E.tab)<-c("E.chr","E.start","E.end")
	pairs.i.refined$Distance<-as.integer(pairs.i.refined$TSS)-round(as.integer(E.tab$E.start)+(as.integer(E.tab$E.end) -as.integer(E.tab$E.start))/2)


	Result.PreSTIGE[[i]]<-pairs.i.refined
}


save(Result.PreSTIGE,file=paste0(dir.result,"Prediction_PreSTIGE_refined.RData"))



### ----- DeepTAC ----- ###
# WRONG ANNOTATION INSIDE FILE! enhancer_name different from combintion of chr,start,end
# pairs.csv: contain the training dataset, don't use it
dir.pred<-"/storage/home/esalviat/MatrixETG/OtherToolPredictions/DeepTACT/Prediction/"
cell.lines<-unique(sapply(strsplit(dir(dir.pred),split="_"),function(x) x[1]))


Result.DeepTAC<-vector("list",length(cell.lines))
names(Result.DeepTAC)<-cell.lines

for(i in 1:length(cell.lines)){

	cat(cell.lines[i],".. ")
	
	name.file.pred<-find.Files(dir.pred,key=c(cell.lines[i],"predictions"))
	#name.file.pairs<-find.Files(dir.pred,key=c(cell.lines[i],"pairs"))
	
	pred.i<-data.table::fread(paste0(dir.pred,name.file.pred))
	

	## convert QTl in ETG, mapping SNP within defned enhancers
	E.tab<-data.table(do.call("rbind",strsplit(pred.i$enhancer_name,split="[:-]")))
	colnames(E.tab)<-c("E.chr","E.start","E.end")
	
	P.tab<-data.table(P.chr=pred.i$promoter_chrom,P.start=pred.i$promoter_start,P.end=pred.i$promoter_end)

	pred.i$distance<-round(as.integer(P.tab$P.start)+(as.integer(P.tab$P.end) -as.integer(P.tab$P.start))/2)-
		round(as.integer(E.tab$E.start)+(as.integer(E.tab$E.end) -as.integer(E.tab$E.start))/2)


	res.i<-data.table(
		E.info=pred.i$enhancer_name,
		P.info=paste0(P.tab$P.chr,":",P.tab$P.start,"-",P.tab$P.end),
		G.symbol=pred.i$promoter_name,
		Predicted=1,
		Distance=pred.i$distance
	)

	ind.take<-which(P.tab$P.chr==E.tab$E.chr & E.tab$E.chr %in% paste0("chr",c(1:22,"X")) )
	res.i<-res.i[ind.take,]



	### --- Refine enhancer list --- ##
	E.tab<-data.table(do.call("rbind",strsplit(res.i$E.info,split="[:-]")))
	colnames(E.tab)<-c("E.chr","E.start","E.end")

	E.GR<-reduce(unique(GRanges(
		seqnames=Rle(E.tab$E.chr),
		ranges=IRanges(start=as.integer(paste0(E.tab$E.start)),end=as.integer(paste0(E.tab$E.end)) ),
		strand=Rle(rep("*",nrow(E.tab)) )
	)))
	E.GR$code<-paste0(E.GR)

	x.ref<-setdiff(E.GR,GR.exon)
	x.ref$code.new<-paste0(x.ref)
	
	ind.ref<-findOverlaps(x.ref,E.GR,type="within")
	x.ref$code.old<-E.GR$code[ind.ref@to]
	x.ref<-x.ref[which(x.ref@ranges@width>=10)]

	pairs.i.refined<-data.table::data.table(E.info.new=x.ref$code.new,E.info=x.ref$code.old)
	
	pairs.i.refined<-merge(pairs.i.refined,res.i,by="E.info",allow.cartesian=TRUE)

	## recompute distance; change name
	colnames(pairs.i.refined)[which(colnames(pairs.i.refined)=="E.info")]<-"E.info.old"
	colnames(pairs.i.refined)[which(colnames(pairs.i.refined)=="Distance")]<-"Distance.old"
	colnames(pairs.i.refined)[which(colnames(pairs.i.refined)=="E.info.new")]<-"E.info"

	E.tab<-data.table(do.call("rbind",strsplit(pairs.i.refined$E.info,split="[:-]")))
	colnames(E.tab)<-c("E.chr","E.start","E.end")

	P.tab<-data.table(do.call("rbind",strsplit(pairs.i.refined$P.info,split="[:-]")))
	colnames(P.tab)<-c("P.chr","P.start","P.end")

	pairs.i.refined$Distance<-round(as.integer(P.tab$P.start)+(as.integer(P.tab$P.end) -as.integer(P.tab$P.start))/2)-round(as.integer(E.tab$E.start)+(as.integer(E.tab$E.end) -as.integer(E.tab$E.start))/2)


	Result.DeepTAC[[i]]<-pairs.i.refined

}

save(Result.DeepTAC,file=paste0(dir.result,"Prediction_DeepTAC_refined.RData"))


