### Create a GenomicRanges file for PanCan ####
# snv position, gene symbol
library(GenomicRanges)
dir.pancan.variants<-"/storage/home/esalviat/MatrixETG/PancanQTL/eQTL/"
dir.result<-"/storage/home/esalviat/MatrixETG/PancanQTL/"


name.pancan<-dir(dir.pancan.variants)

Validated.Pancan<-lapply(name.pancan,function(x) data.table::fread(file=paste0(dir.pancan.variants,x)))
Validated.Pancan<-data.table::rbindlist(Validated.Pancan)
Validated.Pancan<-unique(Validated.Pancan)

GR.Variants<-GenomicRanges::GRanges(
	seqnames=Rle(Validated.Pancan$SNP_chr),
	ranges=IRanges(start=Validated.Pancan$SNP_position,width=1),
	strand=Rle(rep("*",length(nrow(Validated.Pancan)))),
	symbol=Validated.Pancan$egenes 
)

save(GR.Variants,file=paste0(dir.result,"PanCan_hg19_variants_gene_20200305.RData"))
