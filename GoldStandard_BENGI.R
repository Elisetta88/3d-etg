### --- Use BENGI pairs directly --- ###
# we cannnot delete pairs outside TADs

# create enhancer/[promoters files and Matrix of data

dir.enhancer<-"/storage/data/FF/esalviat/ETG/Enhancer/"
dir.promoter<-"/storage/data/FF/esalviat/ETG/Promoter/"
dir.pairs<-"/storage/data/FF/esalviat/ETG/BENGI/"

dir.bengi.variants<-"/storage/home/esalviat/MatrixETG/BENGI/AllPairs_NaturalRatio/"
dir.annotation<-"/storage/home/esalviat/MatrixETG/BENGI/"

CRE.file<-"hg19-cCREs.bed.gz"
TSS.file<-"GENCODEv19-TSSs.bed.gz"


CRE<-data.table::fread(paste0(dir.annotation,CRE.file),header=FALSE)[,-4]
colnames(CRE)<-c("chr","start","end","id","group")

TSS<-data.table::fread(paste0(dir.annotation,TSS.file),header=FALSE)[,-5]
colnames(TSS)<-c("chr","start","end","T.id","strand","G.id")

# Dataset TSS of only most-upstream promoter 
up.tss<- 1500; down.tss<- 500 #[window]
GG<-unique(TSS$G.id)
PP<-pbapply::pblapply(GG,function(x){ 
		sub<-TSS[TSS$G.id==x]
		decreasing<-FALSE
		if( sub$strand[1]=="-" ){ decreasing<-TRUE } 
		sub<-sub[order(sub$start,decreasing=decreasing),]

		if( sub$strand[1]=="+"){
			s.x<-sub$start[1]-up.tss
			e.x<-sub$start[1]+down.tss
		} else {
			s.x<-sub$start[1]-down.tss
			e.x<-sub$start[1]+up.tss
		}
		data.table::data.table(chr=sub$chr[1],start=s.x,end=e.x,strand="*",G.strand=sub$strand[1],G.id=x)
})
TSS<-data.table::rbindlist(PP)


### --- GM12878 --- ###
bengi.files<-dir(dir.bengi.variants)
cell.type<- unique(sapply(strsplit(bengi.files,split="[-\\.]"),function(x) x[[1]]))

for(j in 1:length(cell.type)){

	files.j<-bengi.files[grep(cell.type[j],bengi.files)]
	method.type<-sapply(strsplit(files.j,split="[\\.-]"),function(x) x[2])

	## --- create files --- ##
	## for each method type create: E coordinates, P coordinates, Pairs file
	for(i in 1:length(files.j)){
		data.name<-paste0("Pairs_BENGI_",cell.type[j],"_",method.type[i],".RData")
		cat("\t",data.name,"\n")

		data.i<-data.table::fread(paste0(dir.bengi.variants,files.j[i]),header=FALSE)
		colnames(data.i)<-c("E.id","G.id","flag","cv")

		# match CREs info
		ind.cre<-match(data.i$E.id,CRE$id)
		ind.gene<-match(data.i$G.id,TSS$G.id)

		data.i<-data.table::data.table(data.i,CRE=CRE[ind.cre,],TSS=TSS[ind.gene,])	
		# mantain only 'Enhancer-like' signatures
		data.i<-data.i[data.i$CRE.group=="Enhancer-like",]

		# -- Enhancer -- #
		E.code<-unique(data.i$E.id)
		ind.e<-match(E.code,CRE$id)
		E.i<-data.table::data.table(CRE[ind.e,1:3],strand=rep("*",length(ind.e)),id=CRE$id[ind.e])
		e.name<-paste0("Enhancers_BENGI_",cell.type[j],"_",method.type[i],".bed")

		write.table(E.i,file=paste0(dir.enhancer,e.name),row.names=FALSE,col.names=FALSE,quote=FALSE,sep="\t")

		# -- Promoter -- #
		P.code<-unique(data.i$TSS.G.id)
		ind.p<-match(P.code,TSS$G.id)
		P.i<-TSS[ind.p,]
		p.name<-paste0("Promoter_BENGI_",cell.type[j],"_",method.type[i],".bed")

		write.table(P.i,file=paste0(dir.promoter,p.name),row.names=FALSE,col.names=FALSE,quote=FALSE,sep="\t")
		
		# -- summary -- #
		print(c(Enhancer=nrow(E.i),Promoter=nrow(P.i),Pairs=nrow(data.i)))
		save(data.i,file=paste0(dir.pairs,data.name))

	}
	cat("\n")
}

## Merge data ##

## -- Enhancer -- ##
dir.enhancer<-"/storage/data/FF/esalviat/ETG/Enhancer/"

file.enhancer<-dir(dir.enhancer)[grep("BENGI",dir(dir.enhancer))]
file.enhancer<-file.enhancer[-grep("all",file.enhancer)]
E.definition<-lapply(file.enhancer,function(x,directory) data.table::fread(file=paste0(directory,x)) ,directory=dir.enhancer )
E.definition<-unique(data.table::rbindlist(E.definition))

e.all.name<-paste0("Enhancers_BENGI_all.bed")
write.table(E.definition,file=paste0(dir.enhancer,e.all.name),row.names=FALSE,col.names=FALSE,quote=FALSE,sep="\t")

## -- Promoter -- ##
dir.promoter<-"/storage/data/FF/esalviat/ETG/Promoter/"

file.promoter<-dir(dir.promoter)[grep("BENGI",dir(dir.promoter))]
file.promoter<-file.promoter[-grep("all",file.promoter)]
P.definition<-lapply(file.promoter,function(x,directory) data.table::fread(file=paste0(directory,x)) ,directory=dir.promoter )
P.definition<-unique(data.table::rbindlist(P.definition))

p.all.name<-paste0("Promoter_BENGI_all.bed")
write.table(P.definition,file=paste0(dir.promoter,p.all.name),row.names=FALSE,col.names=FALSE,quote=FALSE,sep="\t")

## -- Pairs -- ##
dir.pairs<-"/storage/data/FF/esalviat/ETG/BENGI/"

file.pairs<-dir(dir.pairs)[grep("BENGI",dir(dir.pairs))]
file.pairs<-file.pairs[-grep("all",file.pairs)]

Pairs<- lapply(file.pairs,function(x,directory) get(load(file=paste0(directory,x))) ,directory=dir.pairs )
Pairs<- data.table::rbindlist(Pairs)
Pairs<- unique(Pairs[,c(1,2)])

pairs.all.name<-paste0("Pairs_BENGI_all.RData")
save(Pairs,file=paste0(dir.pairs,pairs.all.name))

############### --------------- ###############










### Create a GenomicRanges file for BENGI - hg19 ####
# cCRE position(enhancer/pronoter/CTCF), gene symbol, ..
# gencode id used
library(GenomicRanges)

# All pairs: ambiguous mantained - for capture tecnique if TSS of different gene where in the same anchor none of them were discarded
# NaturalRatio: no balanced sets: - all negative pairs identifies are mantained (Fixed: 1 positive to four negative)
dir.bengi.variants<-"/storage/home/esalviat/MatrixETG/BENGI/AllPairs_NaturalRatio/"
dir.annotation<-"/storage/home/esalviat/MatrixETG/BENGI/"

CRE.file<-"hg19-cCREs.bed.gz"
gene.file<-"gencode.v19.annotation.gtf.gz"


bengi.files<-dir(dir.bengi.variants)
cell.type<-sapply(strsplit(bengi.files,split="[\\.-]"),function(x) x[1])
method.type<-sapply(strsplit(bengi.files,split="[\\.-]"),function(x) x[2])

## gene annotation: ftp://ftp.ebi.ac.uk/pub/databases/gencode/Gencode_human/release_19/gencode.v19.annotation.gtf.gz
gencode<-unique(convert.gencode(gene.file,dir.annotation)[,c(1,5)])

## cis-candidate regulatory elements: https://github.com/weng-lab/BENGI/raw/master/Benchmark/Annotations/hg19-cCREs.bed.gz
regulatory<-data.table::fread(cmd=paste('zcat',paste0(dir.annotation,CRE.file)),header=FALSE)
colnames(regulatory)<-c("chr","start","end","rDHS","id","group")
#    CTCF-only Enhancer-like Promoter-like 
#       63.610       989.712        254497

GR.pairs<-get.bengi.pairs(bengi.files,dir.bengi.variants,gencode)

save(GR.pairs,file=paste0(dir.annotation,"BENGI_v3_hg19_cCRE_gene_20200305.RData"))


if(FALSE){
	## some checks

	## Can validated pairs of one cell-type be in negative set of another cell type?
	dir.annotation<-"/storage/home/esalviat/MatrixETG/BENGI/"
	print(load(paste0(dir.annotation,"BENGI_v3_hg19_cCRE_gene_20200305.RData")))

	# select only enhancer-like
	x<-GR.pairs[[1]]
	x<-x[x$group=="Enhancer-like"]

	E.x<-unique(x)
	G.x<-unique(x$G.symbol)

	TP.x<-x[x$type==1]
	TN.x<-x[x$type==0]

	M.TP<-M.TN<-matrix(0,ncol = length(G.x),nrow = length(E.x))

	ind.e.tp<-findOverlaps(TP.x,E.x,type="equal")
	ind.g.tp<-match(TP.x$G.symbol,G.x)
	ind.tp<-matrix(c(ind.e.tp@to,ind.g.tp),ncol=2)
	M.TP[ind.tp]<-1

	ind.e.tn<-findOverlaps(TN.x,E.x,type="equal")
	ind.g.tn<-match(TN.x$G.symbol,G.x)
	ind.tn<-matrix(c(ind.e.tn@to,ind.g.tn),ncol=2)
	M.TN[ind.tn]<-1
	ind.pair<-which( (M.TN+M.TP)==2 ,arr.ind=TRUE )


}







### --- function --- ###

convert.gencode<-function(name.file,path.directory){

	cmd<-paste('zcat',paste0(path.directory,name.file))
	D<-data.table::fread(cmd=cmd,header=FALSE)
	colnames(D)<-c("chr","source","type","start","end","score","strand","phase","additional")

	## mantain only gene annotation
	D<-D[D$type=="gene"]

	# meta data in column additional
	cat("\nProcess: gsub and split..")
	D.new<-strsplit(gsub(";","",gsub('"',"",D$additional)),split=" ")
	cat("\ncreate data.table..\n\n")
	D.new.df<-data.table::data.table(t(sapply(D.new,function(x){
		y<-x[(1:10)*2]
		names(y)<- x[((1:10)*2)-1]
		return(y)
	})))

	return(D.new.df)
}

get.bengi.pairs<-function(name.files,path.directory,gene.symbol,verbose=FALSE){


	Data<-pbapply::pblapply(name.files,function(x,path,gs,verbose){
		
		if(verbose) cat("Let's start:\n")
		
		Di<-data.table::fread(paste0(path,x),header=FALSE)
		colnames(Di)<-c("E.id","G.id","type","cv.group") # cv: cross-validation

		# match gene info
		ind.gene<-match(Di$G.id,gs$gene_id)
		Di$G.symbol<-gs$gene_name[ind.gene]

		# match regulatory region
		ind.reg<-match(Di$E.id,regulatory$id)

		GR.variants<-GenomicRanges::GRanges(
			seqnames=Rle(regulatory$chr[ind.reg]),
			ranges=IRanges(start=regulatory$start[ind.reg],end=regulatory$end[ind.reg]),
			strand=Rle(rep("*",length(ind.reg))),
			group=regulatory$group[ind.reg],
			Di
		)
		
		return(GR.variants)

	},path=path.directory,gs=gene.symbol,verbose=verbose)

	names(Data)<- sapply(strsplit(name.files,split="-"),function(x) x[1])

	return(Data)
}

