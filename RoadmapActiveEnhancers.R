#### Script to compute the master list of active enhancers

## INPUT variables ##
dir.result<-"/storage/home/esalviat/MatrixETG/Roadmap_ChipSeq/"
dir.DNase<-"/storage/home/esalviat/MatrixETG/Roadmap_ChipSeq/DNase/"
dir.H3K27ac<-"/storage/home/esalviat/MatrixETG/Roadmap_ChipSeq/H3K27ac/"
file.annotation<-"/storage/home/esalviat/MatrixETG/Roadmap_ChipSeq/refGene.txt.gz"

header<-c("chr","start","end","name","score","strand","signal","pValue","qValue","peak")
header.annotation<-c("Bin","RefSeq","chr","strand","tx.start","tx.end","cds.start","cds.end","ex.count","ex.start","ex.end","score","protein.id","cds.startstat","cds.endstat","ex.frames")
length.range<-c(10,2500) # checked: 99% are low than 2.500

# qvalue cutoff: based on optimized imrpovment (both overlapping and coverage foreskin E055 E056) 
cutoff<-5
#chromosome<-c()

## library ##
library(data.table)
library(GenomicRanges)

###########################################################################
#### Read files ####
# remove not significant peaks (qvalue<cutoff)
# transform object in genomic regions
Read.Roadmap<-function(filename,colnames,cut){
	x<-fread(file=filename,col.names = colnames)
	x<- x[which(x$qValue >= cut ),]
	GR.x<-GRanges(
		seqnames=Rle(x$chr),
		ranges=IRanges(start=x$start,end=x$end),
		pval=x$pValue,
		qval=x$qValue
	)
	return(GR.x)
}

file.d<-dir(dir.DNase)
file.h<-dir(dir.H3K27ac)

cat(">>> read peaks files <<<\n")
DNase<-lapply(paste0(dir.DNase,file.d),Read.Roadmap,colnames=header,cut=cutoff)
names(DNase)<-sapply(strsplit(file.d,split="-"),function(x) x[[1]])
H3K27ac<-lapply(paste0(dir.H3K27ac,file.h),Read.Roadmap,colnames=header,cut=cutoff)
names(H3K27ac)<-sapply(strsplit(file.h,split="-"),function(x) x[[1]])

## ensure the match between the list
cell.lines<-intersect(names(DNase),names(H3K27ac))
DNase<-DNase[cell.lines]; H3K27ac<-H3K27ac[cell.lines]

## --- Statistics: 2020-05-05 --- ##
if(FALSE){
	
	# -- DNase -- #
	## number of peaks
	range(sapply(DNase,length))
	mean(sapply(DNase,length))
	## length
	mean(unlist(lapply(DNase,function(x) x@ranges@width)))
	
	# -- H3K27ac -- #
	## number of peaks
	range(sapply(H3K27ac,length))
	mean(sapply(H3K27ac,length))
	## length
	range(unlist(lapply(H3K27ac,function(x) x@ranges@width)))
	mean(unlist(lapply(H3K27ac,function(x) x@ranges@width)))

	# -- Cell Specific -- #
	## number of peaks
	range(sapply(CellSpecific.Enhancer.Refined,length))
	mean(sapply(CellSpecific.Enhancer.Refined,length))
	## length
	range(unlist(lapply(CellSpecific.Enhancer.Refined,function(x) x@ranges@width)))
	mean(unlist(lapply(CellSpecific.Enhancer.Refined,function(x) x@ranges@width)))

	# coverage over the DNA
	

}


if(FALSE){
	DL.all<-lapply(names(DNase),function(x) data.frame(Length=DNase[[x]]@ranges@width,Type="DNase") )
	DL.all.red<-Reduce("rbind",DL.all)

	HL.all<-lapply(names(H3K27ac),function(x) data.frame(Length=H3K27ac[[x]]@ranges@width,Type="H3K27ac") )
	HL.all.red<-Reduce("rbind",HL.all)

	## Plot size DNase and H3K27ac peaks ##
	DL<-DL.all.red#data.frame(Length=DNase[[1]]@ranges@width,Type="DNase")
	HL<-HL.all.red#data.frame(Length=H3K27ac[[1]]@ranges@width,Type="H3K27ac")
	EL<-data.frame(Length=Active.Enhancer@ranges@width,Type="Active Enhancer")

	plot<-ggplot(data=rbind(HL,DL,EL),aes(x=Type,y=log2(Length)))+
		geom_violin(aes(fill=Type),alpha=0.5)+
		geom_boxplot(width=0.1,outlier.shape = NA)+#,outlier.alpha=0.2,outlier.size=1)+
		scale_fill_manual(values=c("#ffba92","#ff8080","#8ac6d1"))+
		theme_bw()+theme(legend.position = "none")+xlab("")+ylab("")+
		scale_y_continuous(limits = c(1,15), breaks = log2(c(1,50,100,500,1000,5000,10000,32768)))+
		geom_hline(yintercept = 1,colour="darkgray")

	ggsave(plot,file="/storage/home/esalviat/MatrixETG/Plot/E_length.pdf",width=8,height = 5)

	### Plot number of cell specific enhancers ###
	df<-melt(sapply(CellSpecific.Enhancer,length))
	df<-data.frame(df,CellType=rownames(df))

	tab.code<-read.csv("/storage/home/esalviat/MatrixETG/Backup/Backup_20181108/RoadMapCellTYpe.csv")
	ind<-match(df$CellType,paste(tab.code$Code))
	
	df<-data.frame(df,labels=paste0(tab.code$Category[ind]," (",df$CellType,")"))
	

	plot2<-ggplot(df,aes(x=labels,y=value))+geom_bar(stat = "identity",colour="black",fill="white")+
		theme_bw()+theme(legend.position = "none")+#,axis.text.x = element_text(angle = 90, hjust = 1))+
		xlab("")+ylab("")+ coord_flip() +
		geom_hline(yintercept = ceiling(mean(df$value)),size=0.5,col="blue",linetype="dashed")+
		ylim(c(0,100000))

	ggsave(plot2,file="/storage/home/esalviat/MatrixETG/Plot/E_number.pdf",width=4,height = 10)

	

}


##### EXONS dataset ###############
# Remove overlap with exons from enhnacers regions
# UCSC: http://hgdownload.cse.ucsc.edu/goldenpath/hg19/database/refGene.txt.gz
# File: last update "May 2019"
DB.ucsc<-fread(file = file.annotation)
colnames(DB.ucsc)<-header.annotation # 74.873 annotations
## Number of different exons ## 
# build a database in which each row is an exon
# code.exon: X.Y (X: i-th transcript of original DB.ucsc; Y: j-th exon)
ind.ex<-rep(1:nrow(DB.ucsc),times=DB.ucsc$ex.count)
GR.exon<-GRanges(
		seqnames=Rle(DB.ucsc$chr[ind.ex]),
		ranges=IRanges(start=as.integer(unlist(strsplit(DB.ucsc$ex.start,split=","))),end=as.integer(unlist(strsplit(DB.ucsc$ex.end,split=",")))),
		strand=Rle(rep("*",length(DB.ucsc$chr[ind.ex]))), # no strand to use setdiff
		ind.ucsc=ind.ex,
		code.exon=paste(ind.ex,unlist(lapply(DB.ucsc$ex.count,function(x) 1:x)),sep="."),
		RefSeq=DB.ucsc$RefSeq[ind.ex],
		protein.id=DB.ucsc$protein.id[ind.ex]
		)



##### STEP 1: cell specific enhancer ##### 
# Identify cell specific enhancer as the intersection between Dnase and H3K27ac peaks overlap
CellSpecific.Enhancer<-lapply(seq(length(cell.lines)),function(i,D,H,E,range) {
	x<-intersect(D[[i]],H[[i]])

	### STEP 1a) Remove all enhancers that contains a whole exon
	ind.whole<-findOverlaps(E,x,type="within")
	x.ref1<-x[-unique(ind.whole@to)]

	### STEP 1b) remove exons from enhnacers cell specific definiton
	# Nb. removed all the exons (long non-coding as well)
	# if we remove them from the cell specific definition they will not ppear in the master definition
	x.ref<-setdiff(x.ref1,E)
	
	x.ref<-x.ref[which( x.ref@ranges@width>= range[1] & x.ref@ranges@width<=range[2] )]
	

	return(x.ref)
},D=DNase,H=H3K27ac,E=GR.exon,range=length.range)  
# it doesn't imply that we are discarding these enhancers (only if they are cell specific)
names(CellSpecific.Enhancer)<-cell.lines

if(FALSE){
	# plot number fo cell specific enhancer
	nn<-as.vector(sapply(CellSpecific.Enhancer,length))
	db.nenhancer<-data.frame(CellLine=names(CellSpecific.Enhancer),N.Enhancer=nn)
	plot<-ggplot(data=db.nenhancer,aes(x=CellLine,y=N.Enhancer))+
		geom_bar(fill="white",stat='identity', position='dodge',colour="black", width = 0.7)+
		theme_bw()+
		geom_hline(yintercept = mean(db.nenhancer$N.Enhancer),colour="red",linetype="dashed")+ 
		theme(axis.text.x = element_text(angle = 90, hjust = 1))
	# mean (41.355)

	ggsave(plot,file="/storage/home/esalviat/MatrixETG/Plot_PresentationPadova/N.Enhancer.pdf",width =10,height = 4 )

}


if(FALSE){
	# save cell specific enhancer definition
	# there is not Cell-specific definition for Pancreatic Islet
}


##### STEP 2: master enhancers #### 
# Identify master enhancers as the union of cell specific enhnacers
GR.ALL<-Reduce("append",CellSpecific.Enhancer)
Active.Enhancer<-reduce(GR.ALL,min.gapwidth=15)


# check
#Active.Enhancer
ind.prova<-findOverlaps(Active.Enhancer,GR.exon)
if(length(ind.prova)!=0) warnings("There are active enhnacers within exon regions")


#### STEP 3: remove promoter proximal element
## Promoters priximal annotations: 3.5Kb upstream, 1.5Kb downstream of TSS (approximated as the transcript start, see NOTE)
up.tss<- 3500
down.tss<- 1500
TSS<-data.table(chr=DB.ucsc$chr,start=rep(NA,nrow(DB.ucsc)),end=rep(NA,nrow(DB.ucsc)))
ind.pos<-which(DB.ucsc$strand=="+")
ind.neg<-which(DB.ucsc$strand=="-")

# positive strand
TSS$start[ind.pos]<-DB.ucsc$tx.start[ind.pos]- up.tss
TSS$end[ind.pos]<-DB.ucsc$tx.start[ind.pos] + down.tss
# negative strand
TSS$start[ind.neg]<-DB.ucsc$tx.end[ind.neg] - down.tss
TSS$end[ind.neg]<-DB.ucsc$tx.end[ind.neg] + up.tss

# use "*" as strand to find overlap with enhancer regions
GR.tss<-GRanges(
		seqnames=Rle(TSS$chr),
		ranges=IRanges(start=TSS$start,end=TSS$end),
		strand=Rle(rep("*",nrow(TSS))),
		RefSeq=DB.ucsc$RefSeq,
		protein.id=DB.ucsc$protein.id
		)

if(FALSE){
	## Remove promoter proximal elements from cellspefici enhancer definition
	CellSpecific.Enhancer.Refined<-lapply(CellSpecific.Enhancer,function(x){
		ind.pp<-unique(findOverlaps(x,GR.tss)@from)
		x.Refined<-x[-ind.pp]
		return(x.Refined)
	})

	
}


ind.promoter.proximal<-unique(findOverlaps(Active.Enhancer,GR.tss)@from)
Active.Enhancer.Refined<-Active.Enhancer[-ind.promoter.proximal]
ind.chrM.chrY<-which(paste0(seqnames(Active.Enhancer.Refined)) %in% c("chrY","chrM"))
Active.Enhancer.Refined<-Active.Enhancer.Refined[-ind.chrM.chrY]

### save data in bed format ####
Active.bed<-data.frame(
		chr=as.vector(seqnames(Active.Enhancer.Refined)),
		start=Active.Enhancer.Refined@ranges@start,
		end=Active.Enhancer.Refined@ranges@start+Active.Enhancer.Refined@ranges@width-1,
		strand=paste0(Active.Enhancer.Refined@strand)
	)

name.file<-paste0(dir.result,"Enhancers_N",length(cell.lines),"_qVal",cutoff,"_20200305.bed")
write.table(Active.bed,file=name.file,row.names=FALSE,col.names=FALSE,quote=FALSE,sep="\t")



##### Promoters regions #####
# new promoter definition (based on update annotation)
## Promoters priximal annotations: 0.5Kb upstream, 1.5Kb downstream of TSS (approximated as the transcript start, see NOTE)
up.tss<- 1500
down.tss<- 500
TSS<-data.table(chr=DB.ucsc$chr,start=rep(NA,nrow(DB.ucsc)),end=rep(NA,nrow(DB.ucsc)),tss=rep(NA,nrow(DB.ucsc)))
ind.pos<-which(DB.ucsc$strand=="+")
ind.neg<-which(DB.ucsc$strand=="-")

# positive strand
TSS$start[ind.pos]<-DB.ucsc$tx.start[ind.pos]- up.tss 
TSS$end[ind.pos]<-DB.ucsc$tx.start[ind.pos] + down.tss - 1
TSS$tss[ind.pos]<-DB.ucsc$tx.start[ind.pos]
# negative strand
TSS$start[ind.neg]<-DB.ucsc$tx.end[ind.neg] - down.tss + 1
TSS$end[ind.neg]<-DB.ucsc$tx.end[ind.neg] + up.tss 
TSS$tss[ind.neg]<-DB.ucsc$tx.end[ind.neg]

# use "*" as strand to find overlap with enhancer regions
GR.Promoter<-GRanges(
		seqnames=Rle(TSS$chr),
		ranges=IRanges(start=TSS$start,end=TSS$end),
		strand=Rle(DB.ucsc$strand),
		RefSeq=DB.ucsc$RefSeq,
		protein.id=DB.ucsc$protein.id,
		tss=TSS$tss
		)

## Select only coding promoters (ie, code NM_XXXXX)
# exclude non canonic chromsomes 
ind.NM<-grep("NM",GR.Promoter$RefSeq)
ind.chr<-which(as.vector(GR.Promoter@seqnames) %in% paste0("chr",c(1:22,"X","Y")))
GR.coding.Promoter<-GR.Promoter[intersect(ind.NM,ind.chr),]


## Take only most upstream promoter  as representative of a gene
## NOTE: some genes have transcripts with both negative and positive strand
# each gene could have more than one transcript
#sum(table(table(GR.coding.Promoter$protein.id)))
#[1] 19435 (unique gene)
tab<-table(GR.coding.Promoter$protein.id)
gene.id<-names(tab)

ind.gene.tran<-match(GR.coding.Promoter$protein.id, gene.id)
names(ind.gene.tran)<-gene.id
GRL.Gene <- split(GR.coding.Promoter,ind.gene.tran)
names(GRL.Gene)<-gene.id

GRL.mostupstream<- lapply(GRL.Gene,function(x){
	
	ind.ret<-NA
	if(length(x)==1) ind.ret<-1
	else {
		strand.x<-unique(as.vector(strand(x)))

		if( length(strand.x)!=1 ){
			## if same gene has transcripts with different strands we split it in two
			ind.pos<-which(as.vector(strand(x))=="+")
			ind.neg<-which(as.vector(strand(x))=="-")

			#neg
			xn<-x[ind.neg]
			ind.n<-which((xn@ranges@start+xn@ranges@width-1)==max((xn@ranges@start+xn@ranges@width-1)))[1]			
			#pos
			xp<-x[ind.pos]
			ind.p<-which(xp@ranges@start==min(xp@ranges@start))[1]

			ind.ret<-c(ind.neg[ind.n],ind.pos[ind.p])
			# the negative and positive don't have to overlap
			# otherwise it will be discarded
			if( length( findOverlaps(x[ind.ret[1]],x[ind.ret[2]]) )!=0) ind.ret<-NA

		} else {
			if(strand.x=="-") ind.ret<-which((x@ranges@start+x@ranges@width-1)==max((x@ranges@start+x@ranges@width-1)))[1]
			if(strand.x=="+") ind.ret<-which(x@ranges@start==min(x@ranges@start))[1] 
		}
	}
	return(x[ind.ret])
})

#GR.Promoter.upstream<-Reduce("append",GRL.mostupstream) # not so fast..
Promoter.bed<-lapply(GRL.mostupstream,function(x){
	data.frame(
		chr=as.vector(seqnames(x)),
		start=x@ranges@start,
		end=x@ranges@start+x@ranges@width-1,
		strand=as.vector(strand(x)),
		protein.id=x$protein.id,
		tss=x$tss,
		stringsAsFactors= FALSE	
	)
})
Promoter.bed<-Reduce("rbind",Promoter.bed)
# 19.612 promoters

name.file<-paste0(dir.result,"Promoter_UCSC_mostupstream_all_20200305.bed")
write.table(Promoter.bed,file=name.file,row.names=FALSE,col.names=FALSE,quote=FALSE,sep="\t")


### Check overlap between promoters
## 4.030 promoters have some overlaps with other promoters (not considering the strand)
# Use strand "*": Chip-seq have no strand
GR.PP<-GRanges(
		seqnames=Promoter.bed$chr,
		ranges=IRanges(start=Promoter.bed$start,end=Promoter.bed$end),
		strand=rep("*",nrow(Promoter.bed)),
		protein.id=Promoter.bed$protein.id,
		protein.strand=Promoter.bed$strand,
		tss=Promoter.bed$tss
		)

## added: 2020-03-05 ##
GR.PP.tss<-GRanges(
		seqnames=Promoter.bed$chr,
		ranges=IRanges(start=Promoter.bed$tss-500,end=Promoter.bed$tss+500),
		strand=rep("*",nrow(Promoter.bed)),
		protein.id=Promoter.bed$protein.id,
		tss=Promoter.bed$tss
		)

ind<-findOverlaps(GR.PP.tss)
ind<-ind[ind@from!=ind@to]

GR.nooverlap<-GR.PP[setdiff(1:length(GR.PP),unique(c(ind@from,ind@to)))]

GR.PP.tss.red<-reduce(GR.PP.tss[unique(c(ind@from,ind@to))])
ind.red<-findOverlaps(GR.PP.tss.red,GR.PP.tss)

pp.unique<-unique(ind.red@from)
GR.withoverlap<-lapply(pp.unique,function(i,ii){
	x<-which(i==ii@from)
	y<-ii@to[x]
	gr<-reduce(GR.PP[y])
	gr$protein.id<-paste(GR.PP$protein.id[y],collapse=";")
	gr$protein.strand<-paste(GR.PP$protein.strand[y],collapse=";")
	gr$tss<-paste(GR.PP$tss[y],collapse=";")
	return(gr)
},ii=ind.red)

GR.withoverlap<-Reduce("c",GR.withoverlap)

## genes with positive and negative strand ##
genes.two<- names(which(table(unlist(strsplit(GR.withoverlap$protein.id,split=";")))==2))
check<-lapply(genes.two,function(x) which(GR.PP$protein.id==x) )
all(sapply(check,length)==2)


GR.Promoter.upstream.Refined<-c(GR.nooverlap,GR.withoverlap)
ind.chrM.chrY<-which(paste0(seqnames(GR.Promoter.upstream.Refined)) %in% c("chrY","chrM"))
GR.Promoter.upstream.Refined<-GR.Promoter.upstream.Refined[-ind.chrM.chrY]


## save data in bed format ####
	Promoter.bed<-data.frame(
			chr=as.vector(seqnames(GR.Promoter.upstream.Refined)),
			start=GR.Promoter.upstream.Refined@ranges@start,
			end=GR.Promoter.upstream.Refined@ranges@start+GR.Promoter.upstream.Refined@ranges@width-1,
			strand=rep("*",length(GR.Promoter.upstream.Refined)),
			protein.id=GR.Promoter.upstream.Refined$protein.id,
			prontein.strand=GR.Promoter.upstream.Refined$protein.strand,
			stringsAsFactors= FALSE	
		)


if(FALSE){

	## OLD: all general merging ##
	ind<-findOverlaps(GR.PP,minoverlap=2000)
	ind<-ind[ind@from!=ind@to]
	# second list of merged promoters
	GR.PP.nooverlap<-reduce(GR.PP)
	# 17.882 promoters (1.730 are merged)
	ind.gene<-findOverlaps(GR.PP.nooverlap,GR.PP)

	Genes<-sapply(1:length(GR.PP.nooverlap),function(x,ii,RR){
			sub<-ii[which(ii@from==x)]
			return(paste(RR[sub@to],collapse=";"))
	},ii=ind.gene,RR=GR.PP$protein.id)

	Strands<-sapply(1:length(GR.PP.nooverlap),function(x,ii,RR){
			sub<-ii[which(ii@from==x)]
			return(paste(RR[sub@to],collapse=";"))
	},ii=ind.gene,RR=Promoter.bed$strand)

	## save data in bed format ####
	Promoter.bed<-data.frame(
			chr=as.vector(seqnames(GR.PP.nooverlap)),
			start=GR.PP.nooverlap@ranges@start,
			end=GR.PP.nooverlap@ranges@start+GR.PP.nooverlap@ranges@width-1,
			protein.id=Genes,
			prontein.strand=Strands,
			stringsAsFactors= FALSE	
		)

}


# 18.078 promoters
name.file<-paste0(dir.result,"Promoter_UCSC_mostupstream_merged_20200305.bed")
write.table(Promoter.bed,file=name.file,row.names=FALSE,col.names=FALSE,quote=FALSE,sep="\t")




#####################


gaps.active<-gaps(Active.Enhancer.Refined)
db.gaps<-data.table(width=gaps.active@ranges@width,chr=as.vector(pp@seqnames),
	breaks=cut(gaps.active@ranges@width,breaks=c(0,10,50,1000,3000,+Inf),include.lowest = TRUE))




## Compare with FANTOM
if(FALSE){
	FANTOM<-data.table::fread(file="/storage/home/esalviat/MatrixETG/FANTOM_CAGEseq/human_permissive_enhancers_phase_1_and_2.bed.gz")
	colnames(FANTOM)[1:6]<-c("chr","start","end","ID","score","strand")
	GR.F<-GRanges(
		seqnames=Rle(FANTOM$chr),
		ranges=IRanges(start=FANTOM$start,end=FANTOM$end)
	)


	GR.prova<-reduce(c(ActiveEnhancer,GR.F))
	GR.inter<-intersect(ActiveEnhancer,GR.F)
	
	ind<-findOverlaps(GR.prova,GR.inter)
	res<-tapply(GR.inter@ranges@width[ind@to],ind@from,sum)

	COV<-data.table(
		length=GR.prova@ranges@width,
		overlap=numeric(length(GR.prova))
	)
	
	COV$overlap[as.numeric(names(res))]<-res	
	COV$perc.overlap<-COV$overlap/COV$length
}


# Coverage 


getCoverage<-function(Master,Cell){
		n<-length(Master)
		Result<-data.table(length=Master@ranges@width,number=numeric(n),coverage=numeric(n))
		ind<-findOverlaps(Master,Cell)
		
		## Number of cell specific enhancers within the range of Master enhancer
		tab<-table(ind@from)
		Result$number[as.numeric(names(tab))]<-tab

		## Coverage of master enhancer in the cell type
		# we computed the union so the entire region of a cell enhancer must be contained in a master enhancer
		# moreover all cell specific enhancer must be contained in at least one muster enhancer
		cov<-tapply(Cell@ranges@width[ind@to],ind@from,sum)
		Result$coverage[as.numeric(names(cov))]<-cov

		Result$overlap<-round(Result$coverage/Result$length,3)
		return(Result)
}

InfoSpecificity<-lapply(CellSpecific.Enhancer,function(CC,MM) getCoverage(MM,CC),MM=Active.Enhancer.Refined )


### Save results ###
name.file<-paste0(dir.result,"Enhancers_N",length(cell.lines),"_qVal",cutoff,"_",paste0("beforemerge_min",length.range[1],"max",length.range[2]),".RData")
save(Active.Enhancer.Refined,CellSpecific.Enhancer,InfoSpecificity,file = name.file)




### Check Old promoter list ###
# I don't expect any intersection between active enhancers and promoters
enhancer<-read.table("/storage/home/esalviat/MatrixETG/Roadmap_ChipSeq/Enhancers_N44_qVal5.bed")
colnames(enhancer)<-c("chr","start","end")
promoter<-read.table("/storage/home/esalviat/MatrixETG/Roadmap_ChipSeq/Promoter_UCSC_mostupstream_merged.bed")
colnames(promoter)<-c("chr","start","end","strand","protein.id")



GR.promoter<-GRanges(
		seqnames=promoter$chr,
		ranges=IRanges(start=promoter$start,end=promoter$end),
		strand=rep("*",nrow(promoter))
)
GR.enhancer<-GRanges(
		seqnames=enhancer$chr,
		ranges=IRanges(start=enhancer$start,end=enhancer$end),
		strand=rep("*",nrow(enhancer))
)

ind<-findOverlaps(GR.promoter,GR.enhancer)
ind




